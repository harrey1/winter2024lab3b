public class Wolf{
	
	public Double speed;
	public String name;
	public int weight;
	
	public void goEat(){
		if (this.weight <= 60){
			System.out.println("Feed " + this.name + "!");
		} else {
			System.out.println("Don't overfeed " + this.name + "!!");
		}
	}
	
	public void goChase(){
		if(this.speed >= 5){
			System.out.println(this.name + "! I choose you!");
		} else {
			System.out.println(this.name + ", stay home!");
		}
	}
}