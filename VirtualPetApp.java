import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		Wolf[] pack = new Wolf[4];
		
		for(int i = 0; i < pack.length; i++){
			
			pack[i] = new Wolf();
			
			System.out.println("Give your wolf a name");
			pack[i].name = reader.nextLine();
			System.out.println("Your pet's name is " + pack[i].name);

			System.out.println("How fast is " + pack[i].name + "?");
			pack[i].speed = Double.parseDouble(reader.nextLine());
			System.out.println(pack[i].name + " has a speed of " + pack[i].speed + " km/h");
			
			System.out.println("How much weight does " + pack[i].name + " have?");
			pack[i].weight = Integer.parseInt(reader.nextLine());
			System.out.println(pack[i].name + " has a weight of " + pack[i].weight + " kg");
		}
		
		System.out.println("Here's your last wolf pet's, " + pack[3].name + ", data.");
		System.out.println("Speed: " + pack[3].speed + " km/h");
		System.out.println("Weight: " + pack[3].weight + " kg");
		
		pack[0].goEat();
		pack[0].goChase();
	}
	
}